"""
viewer.py
MNIST image viewer for tensorflow inbuilt test data
kennl@sfu.ca
"""

import tkinter as tk

NUM_PIXELS = 28
MAX_PIXEL = 256
COLOURS = ['#010101'] * NUM_PIXELS


class Viewer(tk.Frame):
    def __init__(self, master=None):
        super().__init__(master)
        self.pack()

    def reviseColourArray(self, intArray):

        hex_strings = []
        hex_array = range(0, MAX_PIXEL, 1)
        for i in hex_array:
            hex_strings.append(format(i, 'x'))

        for i in range(len(intArray)):
            string = ''
            string += '#'
            for j in range(3):
                string += hex_strings[255-intArray[i]]
            COLOURS[i] = string

    def viewer(self, arr2d, count, res='no result'):
        i = 0
        g = self
        g.master.title(
            'MNIST Image ' + str(count) + ' (' + res + ')'
        )
        g.master.geometry('560x620')
        g.master.resizable(False, False)
        g.master.configure(background='black')
        while i < (len(arr2d)):
            self.reviseColourArray(arr2d[i])
            f = tk.Frame(
                g.master,
                background='white',
                width=560,
                border=1,
                height=20,
                relief='ridge'
            )
            f.pack(side='top', anchor='nw')
            for j in range(len(arr2d[i])):
                sf = tk.Frame(
                    f,
                    background=COLOURS[j],
                    width=20,
                    border=1,
                    height=20,
                    relief='ridge'
                )
                sf.pack(side='left', anchor='nw')
            i += 1
        g.mainloop()


class c:
    def __init__(self, x, y=None):
        self.g = Viewer()
        self.x = x
        self.y = y

    def show(self, count):
        self.g.viewer(self.x, count, str(self.y))


if __name__ == "__main__":
    v1 = c([
        [10, 11, 12, 13, 14, 15, 16,
         17, 18, 19, 20, 21, 22, 23,
         24, 25, 26, 27, 28, 29, 30,
         31, 32, 33, 34, 35, 36, 37]
    ])
    v1.show(1)
    v2 = c([
        [10, 11, 12, 13, 14, 15, 16,
         17, 18, 19, 20, 21, 22, 23,
         24, 25, 26, 27, 28, 29, 30,
         31, 32, 33, 34, 35, 36, 37]
    ])
    v2.show(2)
    v3 = c([
        [10, 11, 12, 13, 14, 15, 16,
         17, 18, 19, 20, 21, 22, 23,
         24, 25, 26, 27, 28, 29, 30,
         31, 32, 33, 34, 35, 36, 37]
    ])
    v3.show(3)
