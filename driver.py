"""
driver.py
tensorflow MNIST viewer
kennl@sfu.ca
"""

import os
import viewer as v
import tensorflow as tf
import numpy as np


def inspect_training_data():

    arr = []

    count = 0

    mnist = tf.keras.datasets.mnist

    (x_train, y_train), (x_test, y_test) = mnist.load_data()

    while count < 11:
        arr.append(v.c(
            x_train[count],
            'train: ' + str(y_train[count])
        ))
        arr[count].show(
            count+1
        )
        count += 1


def review_predictions(pred_arr):

    arr = []

    count = 0

    mnist = tf.keras.datasets.mnist

    (x_train, y_train), (x_test, y_test) = mnist.load_data()

    while count < 11:
        arr.append(v.c(
            x_train[count],
            'predict: ' + str(pred_arr[count])
        ))
        arr[count].show(
            count+1
        )
        count += 1


command = 'pip show tensorflow'


def main():

    print(os.system(command))

    mnist = tf.keras.datasets.mnist

    (x_train_, y_train), (x_test, y_test) = mnist.load_data()
    x_train, x_test = x_train_ / 255.0, x_test / 255.0

    model = tf.keras.models.Sequential([
        tf.keras.layers.Flatten(input_shape=(28, 28)),
        tf.keras.layers.Dense(128, activation='relu'),
        tf.keras.layers.Dropout(0.2),
        tf.keras.layers.Dense(10)
    ])

    predictions = model(x_train[:1]).numpy()

    tf.nn.softmax(predictions).numpy()

    loss_fn = tf.keras.losses.SparseCategoricalCrossentropy(from_logits=True)
    loss_fn(y_train[:1], predictions).numpy()

    model.compile(
        optimizer='adam',
        loss=loss_fn,
        metrics=['accuracy']
    )

    model.fit(x_train, y_train, epochs=5)

    model.evaluate(x_test,  y_test, verbose=2)

    probability_model = tf.keras.Sequential([
        model,
        tf.keras.layers.Softmax()
    ])

    probability_model(x_test[:5])

    return model.predict(x_train)


if __name__ == "__main__":
    predictions = []
    res = main()
    for i in range(len(res)):
        predictions.append(np.argmax(res[i]))
    inspect_training_data()
    # review_predictions(predictions)
